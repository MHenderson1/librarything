library(tidyverse)
library(janitor)
library(here)
library(readxl)

here("LibraryThing_matthew.henderson_catalog.xls") %>%
  read_excel() %>%
  clean_names() %>%
  write_rds_bak(data_root("librarything.rds"))
